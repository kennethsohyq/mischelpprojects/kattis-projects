import java.util.ArrayList;
import java.util.Collections;

public class LeSubmission {

    public static void main(String... args) {
        Kattio kattio = new Kattio(System.in);

        int count = kattio.getInt();
        int remaining = kattio.getInt();

        // Generate permutations based on count
        ArrayList<Integer> remain = new ArrayList<>();
        int[] remainSort = new int[remaining];
        int i = 0;
        while (kattio.hasMoreTokens()) {
            int cnt = kattio.getInt(); //Integer.parseInt(scan.nextLine());
            remainSort[i] = cnt;
            remain.add(cnt);
            i++;
        }
        Collections.sort(remain);
        algoLexi(count, remainSort, remain.stream().mapToInt(j -> j).toArray());
    }

    // Generate possibilities algorithmically and based on what you have
    private static void algoLexi(int size, int[] knnArray, int[] check) {
        int i = 1, j=0, k=0;
        for (; i <= size; i++) {
            if (k < check.length && check[k] == i) {
                k++;
                continue;
            }
            for (; j < knnArray.length; j++) {
                int js = knnArray[j];
                if (js > i) {
                    System.out.println(i);
                    break;
                } else {
                    System.out.println(js);
                }
            }
            if (j >= knnArray.length) {
                break;
            }
        }
        for (; j < knnArray.length; j++) {
            System.out.println(knnArray[j]);
        }
        for (; i <= size; i++) {
            System.out.println(i);
        }
    }
}
