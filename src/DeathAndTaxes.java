import java.util.ArrayList;

/**
 * Created by Kenneth on 2/9/2019.
 * for PACKAGE_NAME in Kattis
 */
public class DeathAndTaxes {

    private static final boolean DEBUG = false;

    public static void main(String[] args) {
        // Check if file exists
        ArrayList<String> data = DataHelper.getData("deathtaxes-1.in");
        if (data.isEmpty()) {
            if (DEBUG) System.out.println("Error. No lines found");
            return;
        }

        Mittens mitten = new Mittens();
        while (!mitten.isDead()) {
            String action = data.get(0);
            int x = Integer.parseInt(data.get(1));
            int y;
            data.remove(0);
            data.remove(0);

            switch (action) {
                case "buy":
                    y = Integer.parseInt(data.get(0));
                    data.remove(0);
                    mitten.buy(x, y);
                    break;
                case "sell":
                    y = Integer.parseInt(data.get(0));
                    data.remove(0);
                    mitten.sell(x, y);
                    break;
                case "split":
                    mitten.split(x);
                    break;
                case "merge":
                    mitten.merge(x);
                    break;
                case "die":
                    double finalCharge = mitten.die(x);
                    System.out.println(finalCharge);
                    break;
            }
        }
    }

    static class Mittens {
        private int shares;
        private double avgCost;
        boolean dead;

        public Mittens() {
            this.shares = 0;
            this.avgCost = 0;
            this.dead = false;
        }

        public boolean isDead() {
            return dead;
        }

        public void buy(int shares, int crowns) {
            double totalCrownCost = shares * crowns;
            double presentStockTotal = this.avgCost * this.shares;
            this.shares += shares;
            this.avgCost = (totalCrownCost + presentStockTotal) / this.shares;
        }

        public void sell(int shares, double crowns) {
            this.shares -= shares;
        }

        public void split(int split) {
            this.avgCost /= split;
            this.shares *= split;
        }

        public void merge(int merge) {
            int remainder = this.shares % merge;
            this.avgCost *= merge;
            sell(remainder, this.avgCost);
            this.shares = this.shares / merge;
        }

        public double die(int selloff) {
            this.dead = true;
            double a;
            if (selloff < this.avgCost) {
                // Make a loss. no tax
                a= this.shares * selloff;
            } else {
                // Apply 30% tax
                double profit = selloff - this.avgCost;
                a = this.shares * (selloff - (profit * 0.3));
            }

            return Math.round(a * 100.0) / 100.0;
        }
    }

}
