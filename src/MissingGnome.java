import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Kenneth on 2/9/2019.
 * for PACKAGE_NAME in Kattis
 */
@SuppressWarnings("DuplicatedCode")
public class MissingGnome {

    private static final boolean DEBUG = false;

    public static void main(String... args) {
        ArrayList<String> input = DataHelper.getData("gnomes-0000.in");
        if (input.isEmpty()) {
            if (DEBUG) System.out.println("Error. No lines found");
            return;
        }

        int count = Integer.parseInt(input.get(0));
        input.remove(0); // Leave the remaining lexicons
        input.remove(0);
        if (DEBUG) {
            System.out.println("Remaining Gnomes: ");
            input.forEach(System.out::println); // The Remaining Lexicons
            System.out.println();
        }

        // Generate permutations based on count
        ArrayList<Integer> remain = new ArrayList<>();
        int[] remainSort = new int[input.size()];
        for (int i = 0; i < input.size(); i++) {
            int cnt = Integer.parseInt(input.get(i));
            remainSort[i] = cnt;
            remain.add(cnt);
        }
        Collections.sort(remain);
        algoLexi(count, remainSort, remain.stream().mapToInt(i -> i).toArray());
    }

    // Generate possibilities algorithmically and based on what you have
    private static void algoLexi(int size, int[] knnArray, int[] check) {
        int i = 1, j=0, k=0;
        for (; i <= size; i++) {
            if (k < check.length && check[k] == i) {
                k++;
                continue;
            }
            for (; j < knnArray.length; j++) {
                int js = knnArray[j];
                if (js > i) {
                    System.out.println(i);
                    break;
                } else {
                    System.out.println(js);
                }
            }
            if (j >= knnArray.length) {
                break;
            }
        }
        for (; j < knnArray.length; j++) {
            System.out.println(knnArray[j]);
        }
        for (; i <= size; i++) {
            System.out.println(i);
        }
    }
}
