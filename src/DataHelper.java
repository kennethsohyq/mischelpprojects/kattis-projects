import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Kenneth on 2/9/2019.
 * for PACKAGE_NAME in Kattis
 */
public class DataHelper {

    public static ArrayList<String> getData(String defaultFileName) {
        Kattio sc = getInputScanner(defaultFileName);
        ArrayList<String> data = new ArrayList<>();
        while (sc.hasMoreTokens()) {
            data.add(sc.getWord());
        }
        return data;
    }

    private static Kattio getInputScanner(String defaultFileName) {
        File f = new File("data/" + defaultFileName);
        InputStream is;
        if (f.exists()) {
            try {
                is = new FileInputStream(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                is = System.in;
            }
        }
        else is = System.in;
        return new Kattio(is);
    }
}
