import java.util.ArrayList;

/**
 * Created by Kenneth on 2/9/2019.
 * for PACKAGE_NAME in Kattis
 */
public class TreasureHunt {

    private static final boolean DEBUG = false;

    public static void main(String[] args) {
        // Check if file exists
        ArrayList<String> data = DataHelper.getData("treasurehunt_D.17.in");
        if (data.isEmpty()) {
            if (DEBUG) System.out.println("Error. No lines found");
            return;
        }

        // Get row and column
        int row, column;
        row = Integer.parseInt(data.get(0));
        column = Integer.parseInt(data.get(1));
        data.remove(0); // These are the directions
        data.remove(0);
        if (DEBUG) data.forEach(System.out::println);

        // Validate
        if (row < 1 || row > 200) {
            if (DEBUG) System.out.println("Too few/many rows");
            return;
        }
        if (column < 1 || column > 200) {
            if (DEBUG) System.out.println("Too few/many columns");
            return;
        }

        char[][] map = new char[row][column];

        // Add chars to array
        int r = 0;
        for (String s : data) { map[r] = s.toCharArray(); r++; }

        // Process stuff
        int outcome = attemptFlagRetrieval(map);
        switch (outcome) {
            case LOST:
                System.out.println("Out"); break;
            case LOST_FLAG:
                System.out.println("Lost"); break;
            case ERROR:
                if (DEBUG) System.out.println("An error occurred"); break;
            default:
                System.out.println(outcome); break;
        }
    }

    private static int attemptFlagRetrieval(char[][] map) {
        int steps = 0;
        int rowLoc = 0, colLoc = 0; // Start at (0,0)
        while (true) {
            try {
                char loc = map[rowLoc][colLoc];
                switch (loc) {
                    case 'N': rowLoc--; break;
                    case 'S': rowLoc++; break;
                    case 'E': colLoc++; break;
                    case 'W': colLoc--; break;
                    case 'T': return steps;
                    default: return ERROR;
                }
                steps++;
                // Check if we are going on a loop or cannot find flag
                if (steps > (map.length * map[0].length)) return LOST_FLAG;
            } catch (ArrayIndexOutOfBoundsException e) {
                return LOST;
            }
        }
    }

    private static final int LOST_FLAG = -1, LOST = -2, ERROR = -3;

}
