/**
 * Created by Kenneth on 2/9/2019.
 * for PACKAGE_NAME in Kattis
 */
public class HelloWorld {

    public static void main(String... args) {
        System.out.println("Hello World!");
    }
}
