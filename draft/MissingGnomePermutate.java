import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by Kenneth on 2/9/2019.
 * for PACKAGE_NAME in Kattis
 */
@SuppressWarnings("DuplicatedCode")
public class MissingGnomePermutate {

    private static final boolean DEBUG = true;
    private static final boolean PRINTALL = true;

    public static void main(String... args) {
        ArrayList<String> input = DataHelper.getData("gnomes-0002.in");
        if (input.isEmpty()) {
            if (DEBUG) System.out.println("Error. No lines found");
            return;
        }

        int count, remaining;
        String[] parseGnomeCounts = input.get(0).split(" ");
        count = Integer.parseInt(parseGnomeCounts[0]);
        remaining = Integer.parseInt(parseGnomeCounts[1]);
        input.remove(0); // Leave the remaining lexicons
        if (DEBUG) {
            System.out.println("Remaining Gnomes: ");
            input.forEach(System.out::println); // The Remaining Lexicons
            System.out.println();
        }

        // Generate permutations based on count
        int[] remain = input.stream().mapToInt(Integer::parseInt).toArray();
        int[] possibilities = IntStream.range(1, count+1).toArray();
        //permutatePossibilities(possibilities, remain);
        permutateRecursionLexi(possibilities, 0, remain);
    }

    private static boolean checkMatch(int[] a, int[] match) {
        int needle = 0;
        int size = match.length - 1;
        if (size < 1) return false;
        for (int b : a) {
            if (match[needle] == b) {
                needle++;
                if (needle > size) {
                    if (PRINTALL)
                        System.out.println("Ans Below:");
                    printResult(a);
                    return true;
                }
            }
        }
        return false;
    }

    private static void printResult(int[] res) {
        //Arrays.stream(res).forEach(System.out::println);
        if (DEBUG) {
            Arrays.stream(res).forEach(i -> System.out.print(i + " "));
            System.out.println();
        } else {
            Arrays.stream(res).forEach(System.out::println);
        }
    }

    // Generate possibilities lexiographically
    private static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    private static boolean hasNext(int[] a) {
        int n = a.length;

        // find rightmost element a[k] that is smaller than element to its right
        int k;
        for (k = n-2; k >= 0; k--)
            if (a[k] < a[k+1]) break;
        if (k == -1) return false;

        // find rightmost element a[j] that is larger than a[k]
        int j = n-1;
        while (a[k] > a[j])
            j--;
        swap(a, j, k);

        for (int r = n-1, s = k+1; r > s; r--, s++)
            swap(a, r, s);

        return true;
    }

    private static void permutatePossibilities(int[] a, int[] compare) {
        // print permutations
        if (checkMatch(a, compare)) {
            if (PRINTALL) System.out.println("Ans Below: ");
            printResult(a);
            return;
        }
        if (PRINTALL) printResult(a);
        while (hasNext(a)) {
            if (PRINTALL) printResult(a);
            if (checkMatch(a, compare)) return;
        }
    }

    private static boolean permutateRecursionLexi(int[] a, int start, int[] compare) {
        int size = a.length;
        if (size == start + 1) {
            if (PRINTALL) printResult(a);
            return checkMatch(a, compare);
        } else {
            for (int i = start; i < size; i++) {
                swap(a, start, i);
                boolean result = permutateRecursionLexi(a, start + 1, compare);
                swap(a, start, i);
                if (result) return result;
            }
            return false;
        }
    }

    //Generating permutation using Heap Algorithm
    private static void permutatePossibilitiesHeap(int[] a, int size, int n, int[] compare)
    {
        // if size becomes 1 then prints the obtained
        // permutation
        if (size == 1) {
            // Check if permutation
            if (checkMatch(a, compare)) {
                printResult(a);
                return;
            }
        }

        for (int i=0; i<size; i++)
        {
            permutatePossibilitiesHeap(a, size-1, n, compare);

            // if size is odd, swap first and last
            // element
            if (size % 2 == 1)
            {
                int temp = a[0];
                a[0] = a[size-1];
                a[size-1] = temp;
            }

            // If size is even, swap ith and last
            // element
            else
            {
                int temp = a[i];
                a[i] = a[size-1];
                a[size-1] = temp;
            }
        }
    }
}
