import java.util.ArrayList;

/**
 * Created by Kenneth on 2/9/2019.
 * for PACKAGE_NAME in Kattis
 */
@SuppressWarnings("DuplicatedCode")
public class MissingGnomeAlgorithemicArrayList {

    private static final boolean DEBUG = false;

    public static void main(String... args) {
        ArrayList<String> input = DataHelper.getData("gnomes-0001.in");
        if (input.isEmpty()) {
            if (DEBUG) System.out.println("Error. No lines found");
            return;
        }

        int count, remaining;
        String[] parseGnomeCounts = input.get(0).split(" ");
        count = Integer.parseInt(parseGnomeCounts[0]);
        remaining = Integer.parseInt(parseGnomeCounts[1]);
        input.remove(0); // Leave the remaining lexicons
        if (DEBUG) {
            System.out.println("Remaining Gnomes: ");
            input.forEach(System.out::println); // The Remaining Lexicons
            System.out.println();
        }

        // Generate permutations based on count
        ArrayList<Integer> remain = new ArrayList<>();
        input.stream().mapToInt(Integer::parseInt).forEach(remain::add);
        algoLexi(count, remain);
    }

    private static void printResult(ArrayList<Integer> res) {
        if (DEBUG) {
            res.forEach(i -> System.out.print(i + " "));
            System.out.println();
        } else {
            res.forEach(System.out::println);
        }
    }

    // Generate possibilities algorithmically and based on what you have
    private static void algoLexi(int size, ArrayList<Integer> compare) {
        ArrayList<Integer> orig = (ArrayList<Integer>) compare.clone();
        int sizeOfOrig = orig.size();
        int done = 0;
        for (int i = 1; i <= size; i++) {
            if (done > sizeOfOrig) {
                // Just add to end
                compare.add(i);
                continue;
            }
            if (orig.contains(i)) {
                done++;
                continue;
            }
            boolean slotted = false;
            // Slot in
            if (DEBUG) System.out.println("Testing insertion of " + i);
            if (DEBUG) printResult(compare);
            for (int j = 0; j < compare.size(); j++) {
                if (compare.get(j) > i) {
                    // Value greater than i already, add i before (or at start)
                    slotted = true;
                    compare.add(j, i);
                    break;
                }
            }
            if (!slotted) compare.add(i); // Add to back
        }
        printResult(compare);
    }
}
