import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Kenneth on 2/9/2019.
 * for PACKAGE_NAME in Kattis
 */
@SuppressWarnings("DuplicatedCode")
public class MissingGnomeAlgorithemicQueue {

    private static final boolean DEBUG = false;

    public static void main(String... args) {
        ArrayList<String> input = DataHelper.getData("gnomes-0001.in");
        if (input.isEmpty()) {
            if (DEBUG) System.out.println("Error. No lines found");
            return;
        }

        int count = Integer.parseInt(input.get(0).split(" ")[0]);
        input.remove(0); // Leave the remaining lexicons
        if (DEBUG) {
            System.out.println("Remaining Gnomes: ");
            input.forEach(System.out::println); // The Remaining Lexicons
            System.out.println();
        }

        // Generate permutations based on count
        Queue<Integer> remain = new LinkedList<>();
        for (String s : input) {
            remain.add(Integer.parseInt(s));
        }
        algoLexi(count, remain);
    }

    // Generate possibilities algorithmically and based on what you have
    private static void algoLexi(int size, Queue<Integer> knnQueue) {
        int i = 1;
        for (; i <= size; i++) {
            if (knnQueue.contains(i)) continue;
            while (knnQueue.size() > 0) {
                if (knnQueue.peek() > i) {
                    System.out.println(i);
                    break;
                } else System.out.println(knnQueue.remove());
            }
            if (knnQueue.size() == 0) {
                break;
            }
        }
        for (; i <= size; i++) {
            System.out.println(i);
        }
        while (knnQueue.size() > 0) {
            System.out.println(knnQueue.remove());
        }
    }
}
